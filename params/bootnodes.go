// Copyright 2015 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package params

// MainnetBootnodes are the enode URLs of the P2P bootstrap nodes running on
// the main Ethereum network.
var MainnetBootnodes = []string{
	// EtherCC Foundation Go Bootnodes
	"enode://cd294e3de32275603eeb6413d92f19a9a1b2db743f0f308901faba1e8021ba6b455f5cc39a3d0da4195a05a9bb7967679dad6b95674d506765abb23026293af6@ceres.ethercc-mn.host:27617", //Germany 79.143.185.92
	"enode://5a60797241df381a611afe4c38b4e26d2a7fc1586fbaf8f9d544a7553cda75d838ebfd36e14d04eadbff9cfe1ec34a5d44363bb71265a276da2803b353267cf8@eris.ethercc-mn.host:27617", // USA 148.72.151.8
	"enode://9a7c195bcb1c1c977906b08ef789ce6694bc8925fe3eb2bd1d7ed0f64c0766bf36705d5b914f37060990b08607f1e810d184abd83f1497fe1718baa1d4e35539@makemake.ethercc-mn.host:27617",  // Germany 173.212.218.148
	"enode://449a4990f7e71b6f6ec81fef77857f877da02637a47c0930c7c0b86d1981f3a941eaa5069ad92a2b4376c11f7333805a3a19f245e3045317c8eb761d9e4069dd@haumea.ethercc-mn.host:27617", // Germany 173.249.48.159
	"enode://0b0129a537702fb30213137e233464fea451938c3893b3e298a1cdc05671eb37479cfd134e19d20655fff49328a50539750b30043cc7fecaec650577668b02ec@charon.ethercc-mn.host:27617",// Asia (maintenance)
	"enode://1fcf36941332eefa610e402c5c27d2cf84cb83366b9c5afd74f3dab291805fd65416198279ba6e0c6d5e357c2726c1426c2081a70d6b7547a84d188773d00210@fobos.ethercc-mn.host:27617",// Australia (maintenance)
	"enode://593182123323cfd319e51d19e1c6a961245a8cbef2b2df138325d6dde44c6aa56723ac88307c931ae3bafe88e30310f6c2a35188cda0c502b5e83e5d9db010ab@mercury.ethercc-mn2.host:27617",
	"enode://0fb9da440fec16ec39226d8b8fe27c8c196a7565268f22652766f53f658baf4ca0fcba1746b2aa1d50bcdc7e60982cd45ffd026bcb8fb62dadc648d70bdc2c24@venus.ethercc-mn2.host:27617",
	"enode://c11f9bd76ef6bf00f770fa3963d97d5f1ec56185418cc185d1854e1d0f4a81ebd672130518b821da06463be0ad751528387d6d6aea878e6c0f4f35dac07503e7@earth.ethercc-mn2.host:27617",
	"enode://cdba7bf60f9477428dd388e4a95b6057a46b5056c890007c328b66000921d8724c77bd6f0295fddb5bd086e392ee32c94849d0376ff57171df18f2e57d498203@moon.ethercc-mn2.host:27617",
	"enode://a241fa549d91ef9d4e4be6611b510a39710685450e3b5d738f1e39bf4e67f52c03016fa090ca1e22cd2aef28a6e9f110bd4636c11d3eeafee57edc95821648bd@mars.ethercc-mn2.host:27617",
	"enode://c59f53c3459e8ce0f4b36cbd76323e9d5bd0aa9cefd08eff57431e2f0940d6e9918dbcef175004a162724b531d6ee1787115bba14336d4b2ba29663db9897523@jupiter.ethercc-mn2.host:27617",
	"enode://fb1bc7da9bd05266a68c4a68e70edb532c876d778be40f338d2e4451c5140c29f1d5ae68bb5c23dc48b9d2c2b8a811422e0b50f20eece3bbebb759bba9f114f3@saturn.ethercc-mn2.host:27617",
	"enode://40b6e3e054a3dc5700504624d8dc3d6b9182b83ea7594d9570b33cd6633703ff78e80957abab906ce5dd209dd2abf1c1a22ebe56d8e2da83f4c8adc545bde508@uranus.ethercc-mn2.host:27617",
	"enode://cab6630b94929fcda46e2ef8274590b4d70a489b2db020e059b24147b9b40ff3fd14c589604729b3f4b121f9d77c542328e679d9a13b6790fe28b72bf449438b@neptune.ethercc-mn2.host:27617",
	"enode://05524d7b465263013ac3c7dd2ea84c2f91cdaeafd7fca73dc531083625a658d7af6fe20664e0b163244f168702c2291dde44abcf19b74c81f8a6824291a2ccb5@pluto.ethercc-mn2.host:27617",

	// Ethereum Foundation C++ Bootnodes
	//"enode://979b7fa28feeb35a4741660a16076f1943202cb72b6af70d327f053e248bab9ba81760f39d0701ef1d8f89cc1fbd2cacba0710a12cd5314d5e0c9021aa3637f9@5.1.83.226:30303", // DE
}

// TestnetBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Ropsten test network.
var TestnetBootnodes = []string{
	"enode://0bd007ed2d935959a4fe2dcdba8e379733f1e866843b09c89fd2f43ebeec546763f176dca0fb55db8de9d0a73bc18a62bf216f219a686ae761ed6109d770451c@vesta:.ethercc-mn.host:27617",    // Germany 173.249.48.159
}

// RinkebyBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Rinkeby test network.
var RinkebyBootnodes = []string{
}

// DiscoveryV5Bootnodes are the enode URLs of the P2P bootstrap nodes for the
// experimental RLPx v5 topic-discovery network.
var DiscoveryV5Bootnodes = []string{
}
