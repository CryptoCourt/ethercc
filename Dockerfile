# Build Geth in a stock Go builder container
FROM golang:1.10-alpine as builder

RUN apk add --no-cache make gcc musl-dev linux-headers

ADD . /EtherCC
RUN cd /EtherCC && make gethcc

# Pull Geth into a second stage deploy alpine container
FROM alpine:latest

RUN apk add --no-cache ca-certificates
COPY --from=builder /EtherCC/build/bin/gethcc /usr/local/bin/

EXPOSE 8595 8596 27617 27617/udp
ENTRYPOINT ["gethcc"]
